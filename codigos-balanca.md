# Passo a Passo

1. Acesse `Pasta Compartilhada no OneDrive` → `Planilhas` → `Preços das Balanças`

2. Copie ou Crie o código do produto desejado e vá pra o `Profit`

3. Acesse `Produtos` → `Novo` e cadastre o produto

Observação
-----------------------------------|
Não se esqueça do campo `validade` (Padrão: 5) |
Click do segundo botão após o campo `Barra` para gerar o código para balança. Feito isso, edite o valor, utilizando o código cadastrao na planilha do _Excel_ 

Dica
-----------------------------------|
Na aba de pesquisa, insira o atributo `KG` no terceiro campo após `Cód. Tributação` 

## Enviar carga para balança

1. `Exportação de Dados Para Balança`

2. **Setor**: 1

3. Selecionar Balança → `Gerar`

### Toledo MGV6

1. Macro MT1

### Frisola

```
admin: 1
senha: 1
```

1. `Importar` → `✓ Produtos` -> `Importar`

2. `Transmitir` → `Transmissão Personalizada` → `Link Azul` → `✓ Produtos` → `✓ Frios` → `✓ Balança` → `Transmitir`

### Urano

Observação
--------------------------------------------------------------- |
Exportar todas as balanças novamente no Profit                  |
Executar o programa da balança como administrador               |
Caso der erro, reinstalar o Java 6 presente na pasta da balança |

```
admin: 1
```

1. `Importar Produtos de Outros Sistemas` → `Formato G Misto` → `Limpar Base` → `importar`

2. `Sincronizar Equipamentos` → `>>` → `Sincronizar Agora`









