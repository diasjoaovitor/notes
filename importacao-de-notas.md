# Passo a Passo

1. `APP Ficais` → `Entrada NFe 4.0`

2. Atribua as chaves de pesquisa → `Filtrar`

3. Selecione as notas → `Realizar Entradas de XML`

4. **Condição de Pagamento**: A VISTA → `Importar Notas`

Observação
------------|
Caso exista itens não cadastrados, é necessário fazê-lo
