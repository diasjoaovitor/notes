# Configurações do Sistema Operacional
 
Instalação e configuração do Ubuntu 22.04 no WSL2

## Atualização do Sistema

```
sudo apt update
sudo apt full-upgrade
```
 
## Configuração do Terminal
 
Para isso, vamos instalar as seguintes ferramentas:
 
### [zsh](https://github.com/ohmyzsh/ohmyzsh/wiki/Installing-ZSH)
 
```
sudo apt install zsh
```
 
### [ohmyzshoh](https://github.com/ohmyzsh/ohmyzsh)
 
```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```
 
### [zinit](https://github.com/zdharma-continuum/zinit)
 
```
bash -c "$(curl --fail --show-error --silent --location https://raw.githubusercontent.com/zdharma-continuum/zinit/HEAD/scripts/install.sh)"
```
 
Também vamos habilitar alguns plugins do `zinit`:
 
Primeiro vamos abrir o arquivo de configuração da ferramenta
 
```
code .zshrc
```
 
Agora, basta colar o trecho abaixo no campo indicado:
 
```
### End of Zinit's installer chunk
 
zinit light zdharma/fast-syntax-highlighting
zinit light zsh-users/zsh-autosuggestions
zinit light zsh-users/zsh-completions
```
 
No último passo, vamos aplicar o [Tema Drácula](https://draculatheme.com/windows-terminal), para isso, basta seguir os passos descritos no site.

## Ferramentas Essenciais

### [NodeJS](https://github.com/nodesource/distributions/blob/master/README.md)

```
curl -fsSL https://deb.nodesource.com/setup_lts.x | sudo -E bash -
sudo apt-get install -y nodejs
```

### [YARN](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable)

```
sudo npm install --global yarn
```
