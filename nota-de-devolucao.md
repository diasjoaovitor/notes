# Passo a Passo

## Importar Nota

[Impotação de Notas](https://gitlab.com/diasjoaovitor/notes/-/blob/main/importacao-de-notas.md)

### Atribuir produtos para devolução

Observação
------------|
Sempre confira se o fornecedor está cadastrado e se o CNPJ confere com a Nota Fiscal |
Caso não estiver cadastrado: `Fornecedores` → `Pesquisar` -> `Selecionar` → `Clientes`

1. `Entrada NF` → `Pesquisa` → `Transportar`

Observação
------------|
Caso não estiver com a Nota Fical em mãos, anote o número da nota desejada

2. `Itens da Nota` → `Lançar Devolução do Item` → `Atribuir Quantidade` → `Salvar`

### Emitir a Nota Devolução

[Nota de Devolução](https://gitlab.com/diasjoaovitor/notes/-/blob/main/emissao-nf.md#:~:text=envie%20por%20email-,Devolu%C3%A7%C3%A3o,-Dados%20Adicionais)
