# Emissão de Nota Fiscal

## Passo a Passo

1. `Emissão de NF` → `Novo`

2. **Atribuir campos informados na tabela**: 

| Tipo de Nota                   | CFOP | CFOP 2 |
| ------------------------------ | ---- | ------ |
| 1 - Venda - Saída              | 5101 |        |
| 2 - Referente ao Cupom - Saída | 5929 |        |
| 3 - Devolução - Saída - Bahia  | 5202 | 5202   |
| 3 - Devolução - Saída - Outro  | 6202 | 6202   |
| 4 - Produtor Rural - Entrada   | 1102 |        |

3. **Cliente**: `Duplo click` → `Pesquisar por Nome Fantasia`

4. **Cond. Pagto**: 1

### Venda

5. Vá para `Dados Adicionais` 

6. **Obsevações**: 1

7. **Vendedor**: 1

8. Adicione os ítens manualmente

### Referente ao Cupom 

5. Click em `[F9] Importa Nota Fiscal`

6. **Preencha os campos**: Caixa e COO

7. Click em `Adicionar Cupom` → `Salvar`

8. Vá para `Dados Adicionais` 

10. **Obsevações**: 2

11. **Vendedor**: 1

13. Vá para `App Fiscais` → `GWNF-e` → `Pesquisar`

14. Selecione a nota e click em `Enviar`

15. Depois imprima caso necessário, feche a aba e envie por email

### Devolução

5. `Dados Adicionais`

6. **Obsevações**: 3

7. **Vendedor**: 1

8. `✔ Nota de Devolução` → `Fazer Referência`

9. `NF/Série` → `Nota Fiscal` → `Selecionar` → `Adicionar NF` → `Salvar`

10. Vá para `App Fiscais` → `GWNF-e` → `Pesquisar`

11. Selecione a nota e click em `Enviar`

### Produtor Rural

5. Vá para `Dados Adicionais` 

6. Preencher campos:

| Quantidade | Preço unit. |
|----------- | ------------|

| Observação | 
|----------- | 
| Zerar ICMS |

7. Avance até `Dados da Nota` → `Gerar Nota de Entrada` (Gerar Contas a Pagar: **NÂO**)
